package lab4.facci.edgardopanchana.examen4c;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.util.Calendar;

public class ResultActivity extends AppCompatActivity {

    TextView textViewResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        textViewResultado = (TextView) findViewById(R.id.textViewResultado);

        String semana = getIntent().getExtras().getString("semana");

        Calendar calendario = Calendar.getInstance();
        int semanaActual = calendario.get(Calendar.WEEK_OF_YEAR);
        Log.e("SEMANA ACTUAL", String.valueOf( semanaActual));

        //Si el numero ingresado en la primera pantalla es igual a la semana actual
        //HAS ACERTADO
        //caso contrario INTENTA NUEVAMENTE

        if (Integer.valueOf(semana) == semanaActual){
            //HAS ACERTADO

        }
        else {
            //INTENTA NUEVAMENTE
            textViewResultado.setText("INTENTA NUEVAMENTE");
            textViewResultado.setTextColor(Color.RED);

            textViewResultado.setTextAppearance(this, R.style.EstiloError);
        }

    }
}
