package lab4.facci.edgardopanchana.examen4c;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button buttonRevisar;
    EditText editTextSemana;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        buttonRevisar = (Button) findViewById(R.id.buttonRevisar);
        editTextSemana = (EditText) findViewById(R.id.editTextSemana);

        buttonRevisar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Si el campo esta vacio
                //entonces mostramos error
                //caso contrario llamamos a la activity de resultado

                if (editTextSemana.getText().toString().isEmpty())
                {
                    editTextSemana.setError("Introduce el número de la semana actual");
                    Toast.makeText(
                            getApplicationContext(),
                            "Introduce el número de la semana actual",
                            Toast.LENGTH_LONG).show();
                }
                else {

                    Intent intent = new Intent(
                            MainActivity.this, ResultActivity.class
                    );
                    intent.putExtra("semana", editTextSemana.getText().toString());

                    Log.e("SEMANA", editTextSemana.getText().toString());
                    startActivity(intent);
                }
            }
        });

    }



    @Override
    public void onClick(View v) {
        switch (v){
            case
        }
    }
}
